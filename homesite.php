<?php 
/* 
Template Name: Strona Główna 
*/ 
?>

<?php get_header() ?>

<main id="home">

    <?php get_template_part( 'template-parts/section-front-page' ); ?>

    <?php get_template_part( 'template-parts/section-main-img' ); ?>

    <?php get_template_part( 'template-parts/section-three-square-blocks' ); ?>

    <?php get_template_part( 'template-parts/section-left-right-rep' ); ?>

    <?php get_template_part( 'template-parts/section-background-image' ); ?>

    <?php get_template_part( 'template-parts/section-blog' ); ?>

    <?php get_template_part( 'template-parts/section-newsletter' ); ?>

    <?php get_template_part( 'template-parts/section-bottom-form' ); ?>

</main>




<?php get_footer(); ?>