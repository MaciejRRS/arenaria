<?php 
/* 
Template Name: O firmie
*/ 
?>

<?php get_header() ?>


<?php $imageFrontpageAbout = get_field('about_front_page_img'); ?>
<?php $imageFrontpageAboutMobile = get_field('about_front_page_img_mobile'); ?>

<style>
@media (min-width:772px) {
    .section-front-page {
        background-image: url("<?php echo $imageFrontpageAbout['sizes']['frontpage-cover'];?>");
    }
}

@media (max-width:772px) {
    .section-front-page {
        background-image: url("<?php echo $imageFrontpageAboutMobile['sizes']['frontpage-cover-mobile'];?>");
    }
}
</style>


<main id="o-firmie">
    <section class="section-front-page">
        <div class="section-content">
            <div class="section-text-wrapper">
                <div class="section-subtitle">
                    <p><?php the_field('about_front_page_subtitle');?></p>
                </div>
                <div class="section-title">
                    <h1><?php the_field('about_front_page_title');?></h1>
                </div>
                <div class="section-text">
                    <?php the_field('about_front_page_text');?>
                </div>
                <div class="section-button section-buttons transparent">
                    <a href="<?php the_field('about_front_page_link_1');?>">
                        <button><?php the_field('about_front_page_btn_1');?></button>
                    </a>
                </div>
            </div>
        </div>
        <div class="section-bottom-text">
            <?php the_sub_field('about_front_page_bottom_text');?>
        </div>
        <div class="section-bottom-img">
            <a href="#section-about">
                <img src="/app/themes/arenaria/assets/src/img/circle-1.png" />
            </a>
        </div>
        <div class="section-filtr">
            <img src="/app/themes/arenaria/assets/src/img/section-front-page-filtr.png" />
        </div>
    </section>

    <section class="section-about"
        id="section-about">
        <div class="section-content">
            <div class="section-title">
                <h2><?php the_field('about_middle_title');?></h2>
            </div>
            <div class="section-block">
                <div class="left-block">
                    <?php $imageHeroHomepageLeftRight = get_field('about_middle_img'); ?>
                    <img src="<?php echo $imageHeroHomepageLeftRight['sizes']['large']; ?>"
                        width="<?php echo $imageHeroHomepageLeftRight['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroHomepageLeftRight['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroHomepageLeftRight['alt']); ?>" />
                </div>
                <div class="right-block">
                    <div class="block-text">
                        <?php the_field('about_middle_text');?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-text singlecontent">
        <div class="section-content">
            <?php while(have_posts()) : the_post(); ?>
            <?php the_content();?>
            <?php endwhile; ?>
        </div>
    </section>

    <section class="section-realizacje">
        <div class="section-content">
            <div class="section-title">
                <h2><?php the_field('about_section_realizacje_title');?></h2>
            </div>
        </div>
        <div class="section-content realizacje-posts">

            <?php
                // args query
                $args = array(
                    'post_type' => 'realizacja',
                    'posts_per_page' => 3,
                );

                // custom query
                $recent_posts = new WP_Query($args);

                // check that we have results
                if($recent_posts->have_posts()) : $i=0; ?>

            <?php 
                // start loop
                while ($recent_posts->have_posts() ) : $recent_posts->the_post(); ?>

            <?php $i++;?>

            <div class="post-realizacje-single">
                <a href="<?php the_permalink(); ?>">
                    <div class="post-realizacje"
                        style="background-image: url(<?php the_post_thumbnail_url('large'); ?>)">
                        <div class="post post-<?php echo $i; ?>">

                            <div class="post-content">
                                <div class="content">
                                    <div class="post_date">
                                        <p><?php echo get_the_date('Y'); ?></p>
                                    </div>
                                    <div class="post_title">
                                        <h4><?php echo get_the_title(); ?></h4>
                                        <img class ="no-anim" src="/app/themes/arenaria/assets/src/img/arrow.png" />
                                        <img class ="anim" src="/app/themes/arenaria/assets/src/img/arrow-an.png" />
                                    </div>
                                    <div class="post_text">
                                        
                                        <?php  echo wp_trim_words( get_the_excerpt(), 15 ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>


            <?php endwhile; ?>
            <?php wp_reset_query();?>
            <?php endif;?>


        </div>
        <div class="section-content">
            <div class="section-button">
                <a href="<?php the_field('about_section_realizacje_btn_url');?>">
                    <button><?php the_field('about_section_realizacje_btn');?></button>
                </a>
            </div>
        </div>
    </section>


    <?php get_template_part( 'template-parts/section-bottom-form' ); ?>
</main>

<?php get_footer() ?>