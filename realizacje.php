<?php 
/* 
Template Name: Realizacje
*/ 
?>

<?php get_header() ?>

<main id="realizacje">
    <div class="section-apla" style="background-image: url(<?php the_field("realizacje_img");?>)">
        <img src="/app/themes/arenaria/assets/src/img/realizacje-filtr.png"/>
    </div>
    <div class="section-main">
        <div class="section-title">
            <div class="section-content">
                <h1><?php the_field("realizacje_title");?></h1>
            </div>
        </div>
        <div class="section-search-filter seaarchfilter">
            <div class="section-content">
                <?php the_field("realizacje_search_form");?>
            </div>
        </div>
        <div class="section-seo">
            <div class="section-content">
                <div class="section-title">
                    <h2><?php the_field("realizacje_seo_title");?></h2>
                </div>
                <div class="section-text">
                    <?php the_field("realizacje_seo_text");?>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part( 'template-parts/section-bottom-form' ); ?>
</main>

<?php get_footer() ?>