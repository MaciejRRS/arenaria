<?php 
/* 
Template Name: Kontakt
*/ 
?>

<?php get_header('sub') ?>

<main id="kontakt">

    <section class="section-top">
        <div class="section-content">
            <div class="section-title">
                <h2><?php the_field('kontakt_title');?></h2>
            </div>
        </div>
    </section>

    <section class="section-main">
        <div class="section-content">
            <div class="main-left">
                <div class="section-title">
                    <h4><?php the_field('kontakt_left_title');?></h4>
                </div>
                <div class="left-block">
                    <div class="column">
                        <?php the_field('kontakt_block_left');?>
                    </div>
                    <div class="column column-end">
                        <div class="tel">
                            <img src="/app/themes/arenaria/assets/src/img/phone.png" />
                            <div class="tel-text">
                                <?php the_field('kontakt_block_left_tel');?>
                            </div>
                        </div>
                        <div class="mail">
                            <img src="/app/themes/arenaria/assets/src/img/mail.png" />
                            <div class="mail-text">
                                <?php the_field('kontakt_block_left_mail');?>
                            </div>
                        </div>
                        <div class="social">
                            <?php if( have_rows("kontakt_block_left_social") ): ?>
                            <?php while( have_rows("kontakt_block_left_social") ): the_row();?>
                            <a href="<?php the_sub_field('kontakt_social_url');?>">
                                <img src="<?php the_sub_field('kontakt_social_img');?>" />
                            </a>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="left-text">
                    <?php the_field('kontakt_left_text');?>
                </div>
                <div class="left-bottom">
                    <div class="section-logo">
                        <?php $imageHeroHomepageLeftRight = get_field("footer_logo_img",'options'); ?>
                        <img class="img-hero-homepage"
                            src="<?php echo $imageHeroHomepageLeftRight['sizes']['large']; ?>"
                            width="<?php echo $imageHeroHomepageLeftRight['sizes']['large-width']; ?>"
                            height="<?php echo $imageHeroHomepageLeftRight['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($imageHeroHomepageLeftRight['alt']); ?>" />
                    </div>
                    <div class="section-text">
                        <?php the_field("footer_text",'options');?>
                    </div>
                </div>
            </div>
            <div class="main-right">
                <div class="section-title">
                    <h4><?php the_field('kontakt_right_title');?></h4>
                </div>
                <?php the_field('kontakt_form');?>
            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>