<section class="section-with-tabs"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_sub_field('sec_tab_title');?></h2>
        </div>
        <div class="section-tabs"
            id="section-tabs">
            <?php if( have_rows('sec_tab_rep') ): ?>
            <?php while( have_rows('sec_tab_rep') ): the_row();?>
            <a href="#"
                id='<?php echo get_row_index(); ?>'
                class="mybutton"><?php the_sub_field("sec_tab_rep_title");?></a>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <div class="section-tabs-content"
            id="section-tabs-content">
            <?php if( have_rows('sec_tab_rep') ): ?>
            <?php while( have_rows('sec_tab_rep') ): the_row();?>
            <div id='<?php echo get_row_index(); ?>'>
                <?php the_sub_field("sec_tab_rep_text");?>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

        </div>
        <div class="section-button">
            <a href="<?php the_sub_field('sec_tab_btn_link');?>">
                <button><?php the_sub_field('sec_tab_btn');?></button>
            </a>
        </div>
    </div>
</section>