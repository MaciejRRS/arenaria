<style>
.section-main-img .section-content .point-1 .content:after {
    content: '<?php the_field("sec_main_img_point_1_text");?>';
}

.section-main-img .section-content .point-2 .content:after {
    content: '<?php the_field("sec_main_img_point_2_text");?>';
}

.section-main-img .section-content .point-3 .content:after {
    content: '<?php the_field("sec_main_img_point_3_text");?>';
}
</style>

<section class="section-main-img"
    id="section-main-img"
    style="background-image: url(<?php the_field("sec_main_img");?>)"
    data-aos="fade-up">
    <img src="/app/themes/arenaria/assets/src/img/union1.png"
        id="pins" />
    <div class="section-content">
        <div class="point point-1"
            id='point-1'>
            <div class="content"
                id='content-1'>
                <img src="<?php the_field("sec_main_img_point_1_img");?>" />
            </div>
        </div>
        <div class="point point-2"
            id='point-2'>
            <div class="content"
                id='content-2'>
                <img src="<?php the_field("sec_main_img_point_2_img");?>" />
            </div>
        </div>
        <div class="point point-3"
            id='point-3'>
            <div class="content"
                id='content-3'>
                <img src="<?php the_field("sec_main_img_point_3_img");?>" />
            </div>
        </div>
    </div>
</section>