<section class="section-three-square-blocks"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_field('sec_three_sqr_title1');?></h2>
        </div>
        <div class="section-text">
            <?php the_field('sec_three_sqr_text1');?>
        </div>
        <div class="section-blocks">
            <?php if( have_rows('sec_three_sqr_block1') ): ?>
            <?php while( have_rows('sec_three_sqr_block1') ): the_row();?>
            <div class="block"
                style="background-image: url(<?php the_sub_field("sec_three_sqr_block_back_img_1");?>)">
                <div class="background-filtr"></div>
                <div class="content">
                    <div class="block-icon">
                        <?php $imageIconSqure = get_sub_field('sec_three_sqr_block_icon'); ?>
                        <img class="img-hero-homepage"
                            src="<?php echo $imageIconSqure['sizes']['large']; ?>"
                            width="<?php echo $imageIconSqure['sizes']['large-width']; ?>"
                            height="<?php echo $imageIconSqure['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($imageIconSqure['alt']); ?>" />
                    </div>
                    <div class="block-title">
                        <h3><?php the_sub_field("sec_three_sqr_block_title");?></h3>
                    </div>
                    <div class="block-text">
                        <?php the_sub_field("sec_three_sqr_sqr_text");?>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <?php if( get_field('sec_three_sqr_button_text1') ): ?>
        <div class="section-button center">
            <a
                href="<?php the_field('sec_three_sqr_button_link1') ?>"><button><?php the_field('sec_three_sqr_button_text1') ?></button></a>
        </div>
        <?php endif; ?>
    </div>
</section>