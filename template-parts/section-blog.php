<section class="section-blog"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_field("sec_blog_title");?></h2>
        </div>
        <div class="section-posts">
            <?php
                // args query
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                );

                // custom query
                $recent_posts = new WP_Query($args);

                // check that we have results
                if($recent_posts->have_posts()) : $i=0; ?>

            <?php 
                // start loop
                while ($recent_posts->have_posts() ) : $recent_posts->the_post(); ?>

            <?php $i++;?>

            <a href="<?php the_permalink(); ?>"
                class="post post-<?php echo $i; ?>">
                <div class="post-left">
                    <img src="<?php the_post_thumbnail_url('large'); ?>" />
                </div>
                <div class="post-right">
                    <div class="post_date">
                        <p><?php echo get_the_date('d/m/y'); ?></p>
                    </div>
                    <div class="post_title">
                        <h5><?php echo get_the_title(); ?></h5>
                    </div>
                    <div class="post_text">
                        <?php// echo the_excerpt(); ?>
                        <?php  echo wp_trim_words( get_the_excerpt(), 35 ); ?>
                    </div>
                    <div class="section-button">
                        <button>czytaj więcej >></button>
                    </div>
                </div>
            </a>


            <?php endwhile; ?>
            <?php wp_reset_query();?>
            <?php endif;?>
        </div>
        <?php if( get_field('sec_blog_btn') ): ?>
        <div class="section-button center">
            <a href="<?php the_field("sec_blog_btn_url");?>">
                <button><?php the_field("sec_blog_btn");?></button>
            </a>
        </div>
        <?php endif; ?>
    </div>
</section>