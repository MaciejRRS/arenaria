<section id="<?php the_field('kotwica_link_dla_rolnikaLubDoradcy') ?>" class="text-block-with-image" data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_field('tytul_sekcji_tekst_i_zdjecie_kwadratowe_po_prawej');?></h2>
        </div>

        <div class="section-two-columns">
            <div
                class="block-left  <?php if ( get_field( 'zdjecie_sekcji_tekst_i_zdjecie_kwadratowe_po_prawej' ) ): ?>   <?php else: // field_name returned false ?>   full-width-sec <?php endif; // end of if field_name logic ?>">
                <div class="section-text">
                    <?php the_field('tekst_sekcji_tekst_i_zdjecie_kwadratowe_po_prawej');?>
                </div>
            </div>

            <?php if( get_field('zdjecie_sekcji_tekst_i_zdjecie_kwadratowe_po_prawej') ): ?>
            <div class="block-right">
                <?php $imageRectangle = get_field('zdjecie_sekcji_tekst_i_zdjecie_kwadratowe_po_prawej'); ?>
                <img class="img-rectangle" src="<?php echo $imageRectangle['sizes']['large']; ?>"
                    width="<?php echo $imageRectangle['sizes']['large-width']; ?>"
                    height="<?php echo $imageRectangle['sizes']['large-height']; ?>"
                    alt="<?php echo esc_attr($imageRectangle['alt']); ?>" />
            </div>
            <?php endif; ?>
        </div>

    </div>
</section>