<section
    class="section-form-image-bottom <?php
if( get_field('zmiana_kolejności_img_form__strona_sekcji_ze_zdjeciem_i_formularzem_kontaktowym_kopia2') ) { ?> side-reverse <?php } ?>"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_field('tytuł_sekcji_ze_zdjeciem_i_formularzem_kontaktowym');?></h2>
            <h3><?php the_field('podtytuł_sekcji_ze_zdjeciem_i_formularzem_kontaktowym');?></h3>
        </div>
    </div>

    <div class="bg-bottom-content">
        <div class="section-content">
            <div class="block-left">
                <?php $imageFormBottom = get_field('zdjecie_sekcji_ze_zdjeciem_i_formularzem_kontaktowym_kopia'); ?>
                <img class="imageFormBottom" src="<?php echo $imageFormBottom['sizes']['large']; ?>"
                    width="<?php echo $imageFormBottom['sizes']['large-width']; ?>"
                    height="<?php echo $imageFormBottom['sizes']['large-height']; ?>"
                    alt="<?php echo esc_attr($imageFormBottom['alt']); ?>" />
            </div>

            <div class="block-right">
                <?php echo do_shortcode(get_field('formularz_sekcji_ze_zdjeciem_i_formularzem_kontaktowym')); ?>
            </div>
        </div>
    </div>
</section>