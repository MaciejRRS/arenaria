<section class="section-left-right-rep"
    data-aos="fade-up">
    <div class="section-content">
        <?php if( get_field('sec_left_right_rep_title') ): ?>
        <div class="section-title">
            <h2><?php the_field('sec_left_right_rep_title');?></h2>
        </div>
        <?php endif; ?>
        <?php if( get_field('sec_left_right_rep_text') ): ?>
        <div class="section-text">
            <?php the_field('sec_left_right_rep_text');?>
        </div>
        <?php endif; ?>

        <div
            class="section-blocks <?php if( get_field('enable_block_step_by_step') ) { ?> disable_left_right <?php } ?>">
            <?php if( have_rows('sec_left_right_rep') ): ?>
            <?php while( have_rows('sec_left_right_rep') ): the_row();?>
            <div class="block">
                <div class="block-left">
                    <div class="block-img">
                        <?php $imageHeroHomepageLeftRight = get_sub_field('sec_left_right_rep_img'); ?>
                        <img class="img-hero-homepage"
                            src="<?php echo $imageHeroHomepageLeftRight['sizes']['large']; ?>"
                            width="<?php echo $imageHeroHomepageLeftRight['sizes']['large-width']; ?>"
                            height="<?php echo $imageHeroHomepageLeftRight['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($imageHeroHomepageLeftRight['alt']); ?>" />
                    </div>
                </div>
                <div class="block-right">
                    <div class="block-title">
                        <h3><?php the_sub_field("sec_left_right_rep_subtitle");?></h3>
                    </div>
                    <div class="block-text">
                        <?php the_sub_field("sec_left_right_rep_text");?>
                    </div>
                    <?php if( get_sub_field('sec_left_right_rep_btn') ): ?>
                    <div class="block-button section-button">
                        <a href="<?php the_sub_field('sec_left_right_rep_link');?>">
                            <button><?php the_sub_field('sec_left_right_rep_btn');?></button>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>