<section class="section-faq"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_field('sec_faq_title');?></h2>
        </div>
        <div class="section-blocks">
            <?php if( have_rows('sec_faq_rep') ): ?>
            <?php while( have_rows('sec_faq_rep') ): the_row();?>
            <div class="block">
                <details class="details5">
                    <summary>
                        <p class="faq-block-title"><?php the_sub_field('sec_faq_block_title');?></p>
                    </summary>
                    <?php the_sub_field('sec_faq_block_text');?>
                </details>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>