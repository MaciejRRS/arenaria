<section class="section-background-image" style="background-image: url(<?php the_field("sec_back_img");?>)"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-subtitle">
            <?php the_field('sec_back_subtitle');?>
        </div>
        <div class="section-title">
            <h2><?php the_field("sec_back_title");?></h2>
        </div>
        <div class="section-text">
            <?php the_field("sec_back_text");?>
        </div>
        <div class="section-button center">
            <a href="<?php the_field('sec_back_link');?>">
                <button><?php the_field('sec_back_btn');?></button>
            </a>
        </div>
    </div>
</section>