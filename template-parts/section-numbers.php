<section class="section-numbers"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_field('sec_num_title');?></h2>
        </div>
        <div class="section-numbers-content">
            <?php if( have_rows('sec_num_rep') ): ?>
            <?php while( have_rows('sec_num_rep') ): the_row();?>

            <div class="number">
                <span class="value"><?php the_sub_field('sec_num_main');?></span>
                <p><?php the_sub_field('sec_num_text');?></p>
            </div>

            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>