<section class="section-three-circle-blocks" data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_field('sec_three_circ_title');?></h2>
        </div>
        <div class="wrap-for-revers">
            <?php if( get_field('sec_three_circ_text') ): ?>
            <div class="section-text">
                <?php the_field('sec_three_circ_text');?>
            </div>
            <?php endif; ?>
            <div class="section-blocks">
                <?php if( have_rows('sec_three_circ_block') ): $i = 1?>
                <?php while( have_rows('sec_three_circ_block') ): the_row();?>
                <div class="block after-elements">
                    <div class="block-icon">
                        <?php $imageIcon3blocks = get_sub_field('sec_three_circ_block_icon'); ?>
                        <img src="<?php echo $imageIcon3blocks['sizes']['medium']; ?>"
                            width="<?php echo $imageIcon3blocks['sizes']['medium-width']; ?>"
                            height="<?php echo $imageIcon3blocks['sizes']['medium-height']; ?>"
                            alt="<?php echo esc_attr($imageIcon3blocks['alt']); ?>" />


                    </div>
                    <div class="block-bottom-content">
                        <div class="block-title">
                            <h3><?php the_sub_field("sec_three_circ_block_title");?></h3>
                        </div>
                        <div class="block-text">
                            <?php the_sub_field("sec_three_circ_block_text");?>
                        </div>
                        <span class="number-column"><?php echo $i++; ?></span>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>