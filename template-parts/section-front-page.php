<?php $imageFrontpageDesctop = get_field('sec_front_page_img_1'); ?>
<?php $imageFrontpageMobile = get_field('sec_front_page_img_mobile_1'); ?>
<style>
@media (min-width:772px) {
    .section-front-page {
        background-image: url("<?php echo $imageFrontpageDesctop['sizes']['frontpage-cover'];?>");
    }
}

@media (max-width:772px) {
    .section-front-page {
        background-image: url("<?php echo $imageFrontpageMobile['sizes']['frontpage-cover-mobile'];?>");
    }
}
</style>




<section class="section-front-page">
    <div class="section-content">
        <div class="section-text-wrapper">
            <div class="section-subtitle">
                <p><?php the_field('sec_front_page_subtitle');?></p>
            </div>
            <div class="section-title">
                <?php the_field('sec_front_page_title');?>
            </div>
            <div class="section-text">
                <?php the_field('sec_front_page_text');?>
            </div>
            <div class="section-button section-buttons transparent">
                <a href="<?php the_field('sec_front_page_link_1');?>">
                    <button><?php the_field('sec_front_page_btn_1');?></button>
                </a>
                <a href="<?php the_field('sec_front_page_link_2');?>">
                    <button class="btn-transparent"><?php the_field('sec_front_page_btn_2');?></button>
                </a>
            </div>
        </div>
    </div>
    <div class="section-bottom-text">
        <?php the_field('sec_front_page_bottom_text');?>
    </div>
    <div class="section-bottom-img" id="front-circle">
        <a href="#section-main-img">
            <img src="/app/themes/arenaria/assets/src/img/circle-1.png" />
        </a>
    </div>
    <div class="section-filtr">
        <img src="/app/themes/arenaria/assets/src/img/section-front-page-filtr.png" />
    </div>
</section>