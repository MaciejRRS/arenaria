<section style="background-image: url('/app/themes/arenaria/assets/src/img/bg-opinion.svg')"
    class="section-opinions"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_field('sec_opin_title');?></h2>
        </div>
        <div class="section-slider">
            <div class="swiper mySwiper2">
                <div class="swiper-wrapper">
                    <?php if( have_rows('sec_opin_slider','options') ): ?>
                    <?php while( have_rows('sec_opin_slider','options') ): the_row();?>

                    <div class="swiper-slide">
                        <div class="swiper-text">
                            <?php the_sub_field('sec_opin_text','options'); ?>
                        </div>
                        <div class="swiper-bottom-block">
                            <?php if( get_sub_field('sec_opin_img','options') ): ?>
                            <div class="bottom-block-left">
                                <img src="<?php the_sub_field('sec_opin_img','options'); ?>">
                            </div>
                            <?php endif; ?>
                            <div class="bottom-block-right">
                                <?php the_sub_field('sec_opin_name','options'); ?>
                            </div>
                        </div>
                    </div>

                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>

            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>