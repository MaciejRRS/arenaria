<section class="section-background-image" style="background-image: url(<?php the_field("sec_back_img1");?>)"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-subtitle">
            <?php the_field('sec_back_subtitle1');?>
        </div>
        <div class="section-title">
            <h2><?php the_field("sec_back_title1");?></h2>
        </div>
        <div class="section-text">
            <?php the_field("sec_back_text1");?>
        </div>
        <div class="section-button center">
            <a href="<?php the_field('sec_back_link1');?>">
                <button><?php the_field('sec_back_btn1');?></button>
            </a>
        </div>
    </div>
</section>