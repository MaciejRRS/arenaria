<section class="section-single-block"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_sub_field('sec_sng_blc_title');?></h2>
        </div>
        <div class="section-text">
            <?php the_sub_field('sec_sng_blc_text');?>
        </div>
        <?php if( have_rows('sec_sng_blc_btn') ): ?>
        <div class="section-button center">
            <a class="btn"
                href="<?php the_sub_field('sec_sng_blc_link');?>">
                <?php the_sub_field('sec_sng_blc_btn');?>
            </a>
        </div>
        <?php endif; ?>
    </div>
</section>