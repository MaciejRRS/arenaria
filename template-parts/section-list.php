<section class="section-list"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-title">
            <h2><?php the_field('sec_list_title');?></h2>
        </div>
        <div class="section-text">
            <?php the_field('sec_list_text');?>
        </div>
        <div class="section-list-content">
            <?php if( have_rows('sec_list_rep') ): ?>
            <?php while( have_rows('sec_list_rep') ): the_row();?>

            <p><?php the_sub_field('sec_list_single');?></p>

            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>