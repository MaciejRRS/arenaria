<?php $imageCoverForFarmer = get_field('section-two-blocks-and-background1'); ?>
<section
    style="background-image:linear-gradient(to bottom, rgba(74, 92, 4, 0), rgba(74, 92, 4, 0.67), rgba(0, 0, 0, 0.7)), url(<?php echo $imageCoverForFarmer['sizes']['frontpage-cover'];?>);"
    class="section-two-blocks and-background">



    <div class="section-content">
        <div class="wrap-section-content">
            <div class="left-side">
                <div class="section-subtitle">
                    <h2><?php the_field('sec_two_blocks_title_left_sec_bg1');?></h2>
                    <h2><?php the_field('sec_two_blocks_title_left_sec_bg_center1');?></h5>
                        <h3><?php the_field('sec_two_blocks_title_left_sec_bg_bottom1');?></h3>
                </div>
                <div class="section-text">
                    <?php the_field('sec_two_blocks_text_left_sec_bg1');?>
                </div>
                <div class="section-button center">
                    <a href="<?php the_field('sec_two_blocks_link_left_sec_bg1');?>">
                        <button><?php the_field('sec_two_blocks_btn_left_sec_bg1');?> <svg
                                xmlns="http://www.w3.org/2000/svg" width="15.753" height="15.752"
                                viewBox="0 0 15.753 15.752">
                                <g id="arrow-down-short" transform="translate(-3.234 -10.125)">
                                    <path id="Path_42" data-name="Path 42"
                                        d="M10.454,17.2a1.125,1.125,0,0,1,1.593,0L18,23.159,23.954,17.2A1.126,1.126,0,1,1,25.547,18.8l-6.75,6.75a1.125,1.125,0,0,1-1.593,0l-6.75-6.75a1.125,1.125,0,0,1,0-1.593Z"
                                        transform="translate(-6.89)" fill-rule="evenodd" />
                                    <path id="Path_43" data-name="Path 43"
                                        d="M18,10.125a1.125,1.125,0,0,1,1.125,1.125V22.5a1.125,1.125,0,0,1-2.25,0V11.25A1.125,1.125,0,0,1,18,10.125Z"
                                        transform="translate(-6.889)" fill-rule="evenodd" />
                                </g>
                            </svg></button>
                    </a>
                </div>
            </div>
            <div class="right-side">
                <div class="section-subtitle">
                    <h2><?php the_field('sec_two_blocks_title_right_sec_bg1');?></h2>
                </div>
                <div class="section-text">
                    <?php echo do_shortcode(get_field('sec_two_blocks_shortcode_form_cf71')); ?>
                </div>
            </div>
        </div>

        <div class="section-text">
            <?php the_field('text_under_two_blocks_sec_bg1') ?>
        </div>
    </div>


    <a href="#<?php the_field('kotwica_link_dla_rolnikaLubDoradcy') ?>" class="arrow-scroll"
        id="arenaria-czytaj-wiecej">
        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
            <path id="arrow-circle-down-f"
                d="M16.939,25.061a1.5,1.5,0,0,0,2.121,0L25.425,18.7A1.5,1.5,0,1,0,23.3,16.575l-3.8,3.8V12a1.5,1.5,0,0,0-3,0v8.379l-3.8-3.8A1.5,1.5,0,1,0,10.575,18.7l6.365,6.365ZM18,33A15,15,0,1,1,33,18,15,15,0,0,1,18,33Z"
                transform="translate(-3 -3)" fill="#a5d100" />
        </svg>
    </a>



    <?php $imageFarmer = get_field('img_farmer_two_blocks_sec_bg1'); ?>
    <img class="img-for-farmer" src="<?php echo $imageFarmer['sizes']['large']; ?>"
        width="<?php echo $imageFarmer['sizes']['large-width']; ?>"
        height="<?php echo $imageFarmer['sizes']['large-height']; ?>"
        alt="<?php echo esc_attr($imageFarmer['alt']); ?>" />
</section>