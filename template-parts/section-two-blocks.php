<section class="section-two-blocks"
    data-aos="fade-up">
    <div class="section-content">
        <div class="wrap-section-content">
            <div class="left-side">
                <div class="section-image">
                    <?php $imageHeroHomepageLeft = get_sub_field('sec_two_blocks_img_left'); ?>
                    <img class="img-hero-homepage"
                        src="<?php echo $imageHeroHomepageLeft['sizes']['large']; ?>"
                        width="<?php echo $imageHeroHomepageLeft['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroHomepageLeft['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroHomepageLeft['alt']); ?>" />
                </div>
                <div class="section-subtitle">
                    <h2><?php the_sub_field('sec_two_blocks_title_left');?></h2>
                </div>
                <div class="section-text">
                    <?php the_sub_field('sec_two_blocks_text_left');?>
                </div>
                <div class="section-button center">
                    <a href="<?php the_sub_field('sec_two_blocks_link_left');?>">
                        <button><?php the_sub_field('sec_two_blocks_btn_left');?></button>
                    </a>
                </div>
            </div>
            <div class="right-side">
                <div class="section-image">
                    <?php $imageHeroHomepageRight = get_sub_field('sec_two_blocks_img_right'); ?>
                    <img class="img-hero-homepage"
                        src="<?php echo $imageHeroHomepageRight['sizes']['large']; ?>"
                        width="<?php echo $imageHeroHomepageRight['sizes']['large-width']; ?>"
                        height="<?php echo $imageHeroHomepageRight['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageHeroHomepageRight['alt']); ?>" />
                </div>
                <div class="section-subtitle">
                    <h2><?php the_sub_field('sec_two_blocks_title_right');?></h2>
                </div>
                <div class="section-text">
                    <?php the_sub_field('sec_two_blocks_text_right');?>
                </div>
                <div class="section-button center">
                    <a href="<?php the_sub_field('sec_two_blocks_link_right');?>">
                        <button><?php the_sub_field('sec_two_blocks_btn_right');?></button>
                    </a>
                </div>
            </div>
        </div>

        <div class="section-text">
            <?php the_sub_field('text_under_two_blocks') ?>
        </div>
    </div>
</section>