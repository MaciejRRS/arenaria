<section class="section-newsletter"
    style="background-image: url( <?php the_field("sec_newsletter_img",'options');?> )"
    data-aos="fade-up">
    <div class="section-content">
        <div class="section-subtitle">
            <h3><?php the_field("sec_newsletter_subtitle",'options');?></h4>
        </div>
        <div class="section-title">
            <h2><?php the_field("sec_newsletter_title",'options');?></h2>
        </div>
        <div class="section-text">
            <?php the_field("sec_newsletter_text",'options');?>
        </div>

        <div class="block-newsletter-form">
            <?php echo show_noptin_form(264); ?>
        </div>
    </div>
    </div>
    <div class="section-img">
        <img src="<?php the_field("sec_newsletter_img_right",'options');?>" />
    </div>
</section>