<?php 
/* 
Template Name: Dla doradcy
*/ 
?>

<?php get_header() ?>

<main id="dla-doradcy">

    <?php get_template_part( 'template-parts/section-two-blocks-background1' ); ?>
    <?php get_template_part( 'template-parts/section-text-and-image-rectangle' ); ?>
    <?php get_template_part( 'template-parts/section-three-circle-blocks' ); ?>


    <?php get_template_part( 'template-parts/section-background-image1' ); ?>

    <?php get_template_part( 'template-parts/section-three-square-blocks1' ); ?>

    <?php get_template_part( 'template-parts/section-left-right-rep1' ); ?>


    <?php get_template_part( 'template-parts/section-list' ); ?>

    <?php get_template_part( 'template-parts/section-numbers' ); ?>


    <?php get_template_part( 'template-parts/section-opinions' ); ?>
    <?php get_template_part( 'template-parts/section-faq' ); ?>
    <?php get_template_part( 'template-parts/section-form-image-bottom' ); ?>

</main>




<?php get_footer(); ?>