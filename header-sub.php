<!doctype html>
<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="format-detection"
            content="telephone=no" />
        <meta name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>
            <?php wp_title(); ?>
        </title>

        <link rel="stylesheet"
            href="https://unpkg.com/swiper/swiper-bundle.min.css" />
        <link rel="pingback"
            href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>

    </head>

    <header class="header-sub">

        <div class="top-header">
            <div class="section-content"><?php the_field('gorny_pasek','options') ?>
                <a href="<?php the_field('gorny_pasek_przycisk_link','options') ?>"><?php the_field('gorny_pasek_przycisk_tekst','options') ?>
                    <svg xmlns="http://www.w3.org/2000/svg"
                        width="12.455"
                        height="12.471"
                        viewBox="0 0 12.455 12.471">
                        <g id="arrowhead-right-fill"
                            transform="translate(-7.497 -7.467)">
                            <path id="Path_40"
                                data-name="Path 40"
                                d="M24.029,13.163,19.778,7.827A.892.892,0,1,0,18.39,8.947l3.8,4.776L18.2,18.5a.888.888,0,0,0,1.37,1.13l4.447-5.336a.889.889,0,0,0,.009-1.13Z"
                                transform="translate(-4.274 -0.011)"
                                fill="#fff" />
                            <path id="Path_41"
                                data-name="Path 41"
                                d="M9.278,7.815A.909.909,0,1,0,7.846,8.936l3.842,4.776L7.7,18.479a.891.891,0,0,0,1.37,1.138l4.447-5.336a.889.889,0,0,0,0-1.13Z"
                                transform="translate(0 0)"
                                fill="#fff" />
                        </g>
                    </svg>
                </a>
            </div>
        </div>

        </div>
        <div class="header-container">
            <div class="section-content">
                <div class="header-content">
                    <!-- custom logo start -->
                    <div class="navbar-logo">
                        <?php 
                            $custom_logo_id = get_theme_mod( 'custom_logo' );
                            $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                            ?>
                        <a class="navbar-brand"
                            href="<?php echo esc_url(home_url('/')); ?>"
                            title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                            rel="home"> <img src="<?php echo $image[0]; ?>"
                                alt="<?php echo get_bloginfo( 'name' ) ?>"></a>
                    </div>
                    <!--  custom logo stop -->

                    <div class="navbar-data">
                        <div class="navbar-menu">

                            <?php
                                wp_nav_menu( array(
                                    'theme_location'    => 'primary',
                                    'depth'             => 2,
                                    'container_id'      => 'navbarNavDropdown',
                                    'menu_class'        => 'nav navbar-desktop',
                                    'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker'            =>  new WP_Bootstrap_Navwalker(),
                                    
                                ) );
                                ?>
                        </div>

                        <div class="navbar-hamburger"
                            id="nav-hamburger">
                            <button class="navbar-toggler first-button"
                                type="button"
                                data-target="#navbarNavDropdown-mobile"
                                aria-controls="navbarSupportedContent20"
                                aria-expanded="false"
                                aria-label="Toggle navigation"
                                id="nav-hamburger">
                                <div class="animated-icon1">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </button>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="menu-mobile"
            id="menu-mobile">

            <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'primary',
                                'depth'             => 2,
                                'container_id'      => 'navbarNavDropdown-mobile',
                                'menu_class'        => 'nav navbar-desktop',
                            ) );
                            ?>

        </div>

    </header>

    <body class="<?php the_title();?>">