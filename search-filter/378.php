<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>



<div class="realizacje-content">
    <div class="section-content">
        <?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>
        <div class="post-realizacje-single">
            <a href="<?php the_permalink(); ?>">
                <div class="post-realizacje" style="background-image: url(<?php the_post_thumbnail_url('large'); ?>)">
                    <div class="post post-<?php echo $i; ?>">

                        <div class="post-content">
                            <div class="content">
                                <div class="post_date">
                                    <p><?php echo get_the_date('Y'); ?></p>
                                </div>
                                <div class="post_title">
                                    <h4><?php echo get_the_title(); ?></h4>
                                    <img class="no-anim" src="/app/themes/arenaria/assets/src/img/arrow.png" />
                                    <img class="anim" src="/app/themes/arenaria/assets/src/img/arrow-an.png" />
                                </div>
                                <div class="post_text">
                                    <?php echo the_excerpt(); ?>
                                    <?php//  echo wp_trim_words( get_the_excerpt(), 15 ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>








        <?php
	}
	?>
    </div>
</div>

<!-- Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br /> -->


<?php
}
else
{
	echo "Brak wyników";
}
?>