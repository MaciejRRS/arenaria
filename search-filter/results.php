<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>



<div class="blog-content">
    <div class="content_posts">
        <?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>


        <section class="section-blog">
            <div class="section-content" style="padding:0px">
                <div class="section-posts">


                    <a href="<?php the_permalink(); ?>" class="post post-<?php echo $i; ?>">
                        <div class="post-left">
                            <img src="<?php the_post_thumbnail_url('large'); ?>" />
                        </div>
                        <div class="post-right">
                            <div class="post_date">
                                <p><?php echo get_the_date('d/m/y'); ?></p>
                            </div>
                            <div class="post_title">
                                <h5><?php echo get_the_title(); ?></h5>
                            </div>
                            <div class="post_text">
                                <?php// echo the_excerpt(); ?>
                                <?php  echo wp_trim_words( get_the_excerpt(), 35 ); ?>
                            </div>
                            <div class="section-button">
                                <button>zobacz więcej</button>
                            </div>
                        </div>
                    </a>


                </div>
            </div>
        </section>




        <?php
	}
	?>
    </div>
</div>

<!-- Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br /> -->


<?php
}
else
{
	echo "Brak wyników";
}
?>