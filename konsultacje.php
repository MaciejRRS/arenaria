<?php 
/* 
Template Name: Konsultacje
*/ 
?>

<?php get_header() ?>

<main id="konsultacje">


    <?php $imageCoverConsulting = get_field('zdjecie_tlo_bezplatna_konsultacja'); ?>
    <section
        style="background-image:linear-gradient(to bottom, rgba(74, 92, 4, 0), rgba(74, 92, 4, 0.67), rgba(0, 0, 0, 0.7)), url(<?php echo $imageCoverConsulting['sizes']['frontpage-cover'];?>);"
        class="section-two-blocks-consulting">



        <div class="section-content">
            <div class="wrap-section-content">

                <div class="left-side">
                    <div class="section-subtitle">
                        <h2><?php the_field('tytul_formularza_bezplatna_konsultacja');?></h2>
                    </div>
                    <div class="section-text">
                        <?php echo do_shortcode(get_field('tytul_formularza_bezplatna_konsultacja_kopia')); ?>
                    </div>
                </div>
                <div class="right-side">
                    <?php $imageFarmerKonsult = get_field('zdjecie_obok_formularza_bezplatna_konsultacja'); ?>
                    <img class="img-for-farmer-konsult" src="<?php echo $imageFarmerKonsult['sizes']['large']; ?>"
                        width="<?php echo $imageFarmerKonsult['sizes']['large-width']; ?>"
                        height="<?php echo $imageFarmerKonsult['sizes']['large-height']; ?>"
                        alt="<?php echo esc_attr($imageFarmerKonsult['alt']); ?>" />
                </div>

                <div class="text-bottom-kons">
                    <div class="section-text">
                        <?php the_field('tekst_pod_formularzem_bezplatna_konsultacja') ?>
                    </div>
                </div>
            </div>

        </div>



    </section>

    <section class="text-conulting-free">
        <div class="section-content">
            <div class="section-title">
                <h2><?php the_field('tytul_sekcja_tekstowa_bezplatna_konsultacja') ?></h2>
            </div>
            <div class="section-text">
                <?php the_field('tekst_sekcja_tekstowa_bezplatna_konsultacja_kopia') ?>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>