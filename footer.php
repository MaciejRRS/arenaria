<footer>
    <div class="top-content"
        style="background-image: url(<?php the_field("footer_back_img",'options');?>)">
        <div class="section-content">
            <div class="section-logo">
                <?php $imageHeroHomepageLeftRight = get_field("footer_logo_img",'options'); ?>
                <img class="img-hero-homepage"
                    src="<?php echo $imageHeroHomepageLeftRight['sizes']['large']; ?>"
                    width="<?php echo $imageHeroHomepageLeftRight['sizes']['large-width']; ?>"
                    height="<?php echo $imageHeroHomepageLeftRight['sizes']['large-height']; ?>"
                    alt="<?php echo esc_attr($imageHeroHomepageLeftRight['alt']); ?>" />
            </div>
            <div class="section-text">
                <?php the_field("footer_text",'options');?>
            </div>
        </div>
    </div>
    <div class="bottom-content">
        <div class="section-content">
            <div class="left-side">
                <div class="column">
                    <?php the_field("footer_left_column_1",'options');?>
                </div>
                <div class="column">
                    <?php the_field("footer_left_column_2",'options');?>
                </div>
                <div class="column icons">
                    <?php if( have_rows("footer_left_column_rep",'options') ): ?>
                    <?php while( have_rows("footer_left_column_rep",'options') ): the_row();?>
                    <div class="column">
                        <div class="column-img">
                            <img src="<?php the_sub_field("footer_left_column_img",'options');?>" />
                        </div>
                        <div class="column-text">
                            <?php the_sub_field("footer_left_column_text",'options');?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="right-side">
                <div class="columns">
                    <?php if( have_rows("footer_rep_column",'options') ): ?>
                    <?php while( have_rows("footer_rep_column",'options') ): the_row();?>
                    <div class="column">
                        <?php if( have_rows("footer_right_column_rep_rep",'options') ): ?>
                        <?php while( have_rows("footer_right_column_rep_rep",'options') ): the_row();?>

                        <a href="<?php the_sub_field("footer_right_column_single_url",'options');?>">
                            <?php the_sub_field("footer_right_column_single",'options');?>
                        </a>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="social">
                    <?php if( have_rows("footer_right_column_icons",'options') ): ?>
                    <?php while( have_rows("footer_right_column_icons",'options') ): the_row();?>
                    <a href="<?php the_sub_field("footer_icons_url",'options');?>">
                        <img src="<?php the_sub_field("footer_icons_img",'options');?>" />
                    </a>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="rr">
        <div class="section-content">
            <div class="left-rr">
                <a href="https://redrocks.pl/">
                    RedRockS ® - Strony Internetowe
                </a>
            </div>
            <div class="right-rr">
                <p>Arenaria © 2021</p>
            </div>
        </div>
    </div>
</footer>

<!-- Swiper JS -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- Initialize Swiper -->
<script>
var swiper = new Swiper(".mySwiper2", {
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    loop: true,
    autoplay: {
        delay: 3000,
        disableOnInteraction: false,
    },
    centeredSlides: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    breakpoints: {
        640: {
            slidesPerView: 1,
            spaceBetween: 5,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 5,
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        1920: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
    },
});

var swiper = new Swiper(".mySwiper-single", {
    loop: true,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        // when window width is >= 320px
        0: {
            slidesPerView: 1,
            spaceBetween: 20
        },
        // when window width is >= 692px
        920: {
            slidesPerView: 2
        }
    }
});
</script>

<script>
jQuery(document).ready(function($) {
    $("#section-tabs a:first-child").addClass("active");
    $("#section-tabs-content div").css("display", "none");
    $("#section-tabs-content div:first-child").css("display", "block");
    $("#section-tabs a").click(function(evt) {
        var id = $(this).attr("id");
        $("#section-tabs-content div").css("display", "none");
        $("#section-tabs a").removeClass("active");
        $("#section-tabs-content div#" + id + "").css("display", "block");
        $("#section-tabs a#" + id + "").addClass("active");
        evt.preventDefault();
    });
});


jQuery(document).ready(function($) {
    $(window).on('load scroll resize orientationchange', function() {
        var scroll = $(window).scrollTop();
        var $win = $(window);
        if (scroll >= 1) {
            $(".header-container").addClass("scroll");
        } else {
            $(".header-container").removeClass("scroll");
        }
    });
});
jQuery(document).ready(function($) {
    $(window).scroll(testScroll);
    var viewed = false;

    function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    function testScroll() {
        if (isScrolledIntoView($(".section-numbers")) && !viewed) {
            viewed = true;
            $('.value').each(function() {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 4000,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        }
    }
});

jQuery(document).ready(function($) {
    $("#front-circle").hover(function(evt) {
        $("#front-circle").addClass('move');
    });
});

jQuery(document).ready(function($) {
    $('#front-circle a').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(sectionTo).offset().top - 200
        }, 100);
    });
});

jQuery(document).ready(function($) {
    $(window).width();
    var width = $(window).width();
    $('#front-circle').on('click', function() {
        if (width >= 772) {
            $('#front-circle').addClass('first');
            $('#point-1').addClass('appear');
            $('#point-1').on("animationend", function() {
                $('#point-1').addClass('move-on');
                $('#point-3').addClass('move-on-3');
                $('#point-1').on("animationend", function() {
                    $('#point-2').addClass('appear');
                    $('#point-2').on("animationend", function() {
                        $('#pins').addClass('appear');
                    });
                });
            });
            $('#point-3').addClass('appear');
        } else {
            $('#front-circle').addClass('first');
            $('#point-1').addClass('appear');
            $('#point-1').on("animationend", function() {
                $('#point-1').addClass('move-on-mobile');
                $('#point-3').addClass('move-on-3-mobile');
                $('#point-1').on("animationend", function() {
                    $('#point-2').addClass('appear');
                    $('#point-2').on("animationend", function() {
                        $('#pins').addClass('appear');
                    });
                });
            });
            $('#point-3').addClass('appear');
        }
    });
});

jQuery(document).ready(function($) {
    $(window).width();
    var width = $(window).width();
    $(document).on('scroll', function() {
        if (width >= 772) {
            if ($(this).scrollTop() >= $('#section-main-img').position().top - 500) {
                $('#front-circle').addClass('first');
                $('#point-1').addClass('appear');
                $('#point-1').on("animationend", function() {
                    $('#point-1').addClass('move-on');
                    $('#point-3').addClass('move-on-3');
                    $('#point-1').on("animationend", function() {
                        $('#point-2').addClass('appear');
                        $('#point-2').on("animationend", function() {
                            $('#pins').addClass('appear');
                        });
                    });
                });
                $('#point-3').addClass('appear');
            }
        } else {

            if ($(this).scrollTop() >= $('#section-main-img').position().top - 500) {
                $('#front-circle').addClass('first');
                $('#point-1').addClass('appear');
                $('#point-1').on("animationend", function() {
                    $('#point-1').addClass('move-on-mobile');
                    $('#point-3').addClass('move-on-3-mobile');
                    $('#point-1').on("animationend", function() {
                        $('#point-2').addClass('appear');
                        $('#point-2').on("animationend", function() {
                            $('#pins').addClass('appear');
                        });
                    });
                });
                $('#point-3').addClass('appear');
            }

        }
    });
});

jQuery(document).ready(function() {
    jQuery("#noptin-form-1__field-potwierdzenie").click(function() {
        jQuery("#noptin-form-1__field-potwierdzenie").toggleClass("active");
    });
});
</script>

<?php wp_footer(); ?>




<script>
AOS.init();
</script>



</body>

</html>