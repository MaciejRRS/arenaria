<?php get_header('sub') ?>
<style>
:root {
    --swiper-theme-color: #a5d100 !important;
}
</style>
<main class="singlepost">
    <div class="section-top">
        <div class="section-content">
            <div class="top-btn">
                <a href="/artykuly">
                    <button> >> WRÓĆ DO ARTYKUŁÓW</button>
                </a>
            </div>
            <div class="top-block">
                <div class="left-block">
                    <img src="<?php the_post_thumbnail_url('large'); ?>" />
                </div>
                <div class="right-block">
                    <div class="post_date">
                        <p><?php echo get_the_date('d/m/y'); ?></p>
                    </div>
                    <div class="post_title">
                        <h1><?php echo get_the_title(); ?></h1>
                    </div>
                    <div class="post_cat">
                        <?php
                            $category_detail = get_the_category(get_the_ID());
                            $cat_arr = [];
                            foreach ($category_detail as $cd)
                            {
                                $cat_arr[] = $cd->cat_name;
                            }
                            ?>
                        <p> <?php echo $cat_arr[0];?></p>
                        <p> <?php echo $cat_arr[1];?></p>
                        <p> <?php echo $cat_arr[2];?></p>
                    </div>
                    <div class="post_text">
                        <?php  echo wp_trim_words( get_the_excerpt(), 35 ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-middle singlecontent">
        <div class="section-content">
            <?php while(have_posts()) : the_post(); ?>
            <?php the_content();?>
            <?php endwhile; ?>
        </div>
    </div>
    <div class="section-button single-btn-back">
        <div class="section-content">
            <a href="/artykuly">
                <button> >> WRÓĆ DO ARTYKUŁÓW</button>
            </a>
        </div>
    </div>
    <div class="section-bottom">
        <div class="section-title">
            <div class="section-content">
                <h2><?php the_field('single_slider_title');?></h2>
            </div>
        </div>
        <div class="section-slider">
            <div class="section-content">
                <div class="swiper mySwiper-single">
                    <div class="swiper-wrapper">
                        <?php if( have_rows("single_wybierz_post") ): ?>
                        <?php while( have_rows("single_wybierz_post") ): the_row();?>
                        <?php
                                $featured_post = get_sub_field('single_wybrany_post');
                                $permalink = get_permalink( $featured_post->ID );
                                $date = get_the_date( 'l F j, Y', $featured_post->ID );
                        ?>
                        <div class="swiper-slide">
                            <div class="post">
                                <a class="post_link"
                                    href="<?php echo esc_url( $permalink ); ?>">
                                    <div class="post-left">
                                        <?php echo get_the_post_thumbnail($featured_post->ID ); ?>
                                    </div>
                                    <div class="post-right">
                                        <div class="post_date">
                                            <p><?php echo esc_html( $date ); ?></p>
                                        </div>
                                        <div class="post_title">
                                            <h3><?php echo esc_html( $featured_post->post_title ); ?></h3>
                                        </div>
                                        <div class="post_button">
                                            <button> CZYTAJ WIĘCEJ >> </button>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>


                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>

        </div>
    </div>
    <?php get_template_part( 'template-parts/section-bottom-form' ); ?>

</main>
<?php get_footer(); ?>