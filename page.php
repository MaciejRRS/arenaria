<?php get_header('sub'); ?>

<main id="default-page">
    <div class="section-content">
        <?php
		// Start the loop.
		while ( have_posts() ) : the_post();
				
		// Include the page content template.
		//get_template_part( 'template-parts/content', 'page' );
						?>
        <h1><?php the_title(); ?></h1>
        <div class="content-default-page">
            <?php the_content(); ?>
        </div>
        <?php //If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) {
								comments_template();
						}
				
						endwhile;
		?>
    </div>
</main><!-- .site-main -->

<?php get_footer(); ?>