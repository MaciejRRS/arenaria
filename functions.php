<?php

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';


register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'arenaria' ),
) );


/*
	 * Enable support for custom logo.
	 *
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 400,
		'width'       => 100,
		'flex-height' => true,
		'flex-width'  => true,
	) );

// Register thumbnails
	add_theme_support( 'post-thumbnails' );
    add_image_size( 'frontpage-cover', 1920, 1080 ); // Soft Crop
    add_image_size( 'frontpage-cover-mobile', 772, 900 ); // Soft Crop

    

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}


function add_stylesheets_and_scripts()
{
wp_enqueue_style( 'style', get_stylesheet_uri(), array(), filemtime(get_template_directory() . '/style.css'), 'all' );
wp_enqueue_style('custom', get_template_directory_uri() . '/assets/dist/css/main.min.css', array(), filemtime(get_template_directory() . '/assets/dist/css/main.min.css'), 'all' );

wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/src/js/libr/jquery.min.js', array(), null, true);
wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/src/js/libr/bootstrap.bundle.min.js', array(), null, true);
wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/src/js/main.js', array(), null, true );

global $template;

// only for o-firmie.php hompage.php, 

 if ((basename($template) === 'o-firmie.php') || (basename($template) === 'dla-rolnika.php') ){
     wp_enqueue_style( 'swiper-css', get_template_directory_uri() . '/assets/src/css/swiper/swiper-bundle.min.css' );
     wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/assets/src/js/swiper/swiper-bundle.min.js', array(), null, true );
     wp_enqueue_script( 'sctipts-about', get_template_directory_uri() . '/assets/src/js/swiper/scripts-about.js', array(), null, true );
 }

// add animate AOS for all pages
    wp_enqueue_style( 'aos-css', get_template_directory_uri() . '/assets/src/css/aos.css' );
    wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/assets/src/js/aos.js', array(), null, true );


}
add_action('wp_enqueue_scripts', 'add_stylesheets_and_scripts');


    
//add option page to panel (ACF)
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Newsletter');
	acf_add_options_page('Stopka');
    acf_add_options_page('Opinie');
    acf_add_options_page('Górny pasek');
}


// delete p from contact form 7
add_filter('wpcf7_autop_or_not', '__return_false');


//** *Enable upload for webp image files.*/
function webp_upload_mimes($existing_mimes) {
    $existing_mimes['webp'] = 'image/webp';
    return $existing_mimes;
}
add_filter('mime_types', 'webp_upload_mimes');

//** * Enable preview / thumbnail for webp image files.*/
function webp_is_displayable($result, $path) {
    if ($result === false) {
        $displayable_image_types = array( IMAGETYPE_WEBP );
        $info = @getimagesize( $path );

        if (empty($info)) {
            $result = false;
        } elseif (!in_array($info[2], $displayable_image_types)) {
            $result = false;
        } else {
            $result = true;
        }
    }

    return $result;
}
add_filter('file_is_displayable_image', 'webp_is_displayable', 10, 2);

function filter_acf_the_content( $value ) {
if ( class_exists( 'iworks_orphan' )) {
$orphan = new iworks_orphan();
$value = $orphan->replace( $value );
}
 
return $value;
};



// deregister search & filter css i js
function remove_sf_scripts() {
	if ( !is_page(array(157,163) ) ) {
		wp_deregister_script( 'search-filter-plugin-build' );
		wp_deregister_script( 'search-filter-plugin-chosen' );
		wp_deregister_script( 'jquery-ui-datepicker' );
		wp_deregister_style( 'search-filter-plugin-styles' );
	}
}
add_action('wp_enqueue_scripts', 'remove_sf_scripts', 100);






add_filter('bcn_breadcrumb_url', 'my_breadcrumb_url_changer', 3, 10);
function my_breadcrumb_url_changer($url, $type, $id)
{
    if(in_array('sections', $type))
    {
        $url = str_replace("sections/", "", $url);
    }
    return $url;
}

//custom post
function custom_post_type_realizacje() {

    $labels = array(
    
        'name'                  => _x( 'Realizacje', 'Post Type General Name', 'Arenaria' ),
    
        'singular_name'         => _x( 'Realizacja', 'Post Type Singular Name', 'Arenaria' ),
    
        'menu_name'             => __( 'Nasze Realizacje', 'Arenaria' ),
    
        'name_admin_bar'        => __( 'Realizacje', 'Arenaria' ),
    
        'archives'              => __( 'Archiwum', 'Arenaria' ),
    
        'attributes'            => __( 'Właściwości', 'Arenaria' ),
    
        'parent_item_colon'     => __( 'Rodzic:', 'Arenaria' ),
    
        'all_items'             => __( 'Wszystkie realizacje', 'Arenaria' ),
    
        'add_new_item'          => __( 'Dodaj nową realizację', 'Arenaria' ),
    
        'add_new'               => __( 'Dodaj nową realizację', 'Arenaria' ),
    
        'new_item'              => __( 'Nowa realizacja', 'Arenaria' ),
    
        'edit_item'             => __( 'Edytuj realizacje', 'Arenaria' ),
    
        'update_item'           => __( 'Zaktualizuj', 'Arenaria' ),
    
        'view_item'             => __( 'Zobacz', 'Arenaria' ),
    
        'view_items'            => __( 'Zobacz Realizacje', 'Arenaria' ),
    
        'search_items'          => __( 'Szukane', 'Arenaria' ),
    
        'not_found'             => __( 'Nie znaleziono', 'Arenaria' ),
    
        'not_found_in_trash'    => __( 'Nie znaleziono w koszu', 'Arenaria' ),
    
        'featured_image'        => __( 'Obrazek realizacji', 'Arenaria' ),
    
        'set_featured_image'    => __( 'Ustaw obrazek realizacji', 'Arenaria' ),
    
        'remove_featured_image' => __( 'Usuń obrazek realizacji', 'Arenaria' ),
    
        'use_featured_image'    => __( 'Ustaw jako obrazek realizacji', 'Arenaria' ),
    
        'insert_into_item'      => __( 'Wstaw do realizacji', 'Arenaria' ),
    
        'uploaded_to_this_item' => __( 'Załadowane do wpisu', 'Arenaria' ),
    
        'items_list'            => __( 'Lista elementów', 'Arenaria' ),
    
        'items_list_navigation' => __( 'Nawigacja', 'Arenaria' ),
    
        'filter_items_list'     => __( 'Filtrowanie', 'Arenaria' ),
    
    );
    
    $args = array(
    
        'label'                 => __( 'Nasze Realizacje', 'Arenaria' ),
    
        'description'           => __( 'Strony poj. realizacji', 'Arenaria' ),
    
        'labels'                => $labels,
    
        'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
    
        'taxonomies'            => array( 'category_trainings','category_trainings2' ),
    
        'hierarchical'          => false,
    
        'public'                => true,
    
        'show_ui'               => true,
    
        'show_in_menu'          => true,
    
        'menu_position'         => 25,
    
        'show_in_admin_bar'     => true,
    
        'show_in_nav_menus'     => true,
    
        'can_export'            => true,
    
        'has_archive'           => false,
    
        'exclude_from_search'   => false,
    
        'publicly_queryable'    => true,
    
        'capability_type'       => 'post',
    
    );
    
    register_post_type( 'realizacja', $args );
    
    }

add_action( 'init', 'custom_post_type_realizacje', 0 );

function category_realizacje() {

    $labels = array(
    
        'name'                       => _x( 'Kategoria realizacji', 'Taxonomy General Name', 'Arenaria' ),
    
        'singular_name'              => _x( 'Kategoria realizacji', 'Taxonomy Singular Name', 'Arenaria' ),
    
        'menu_name'                  => __( 'Kategorie realizacji', 'Arenaria' ),
    
        'all_items'                  => __( 'Wszystkie realizacje', 'Arenaria' ),
    
        'parent_item'                => __( 'Roddzic', 'Arenaria' ),
    
        'parent_item_colon'          => __( 'Rodzic:', 'Arenaria' ),
    
        'new_item_name'              => __( 'Nowa kategoria', 'Arenaria' ),
    
        'add_new_item'               => __( 'Dodaj kategorie', 'Arenaria' ),
    
        'edit_item'                  => __( 'Edytuj', 'Arenaria' ),
    
        'update_item'                => __( 'Zaktualizuj', 'Arenaria' ),
    
        'view_item'                  => __( 'Zobacz', 'Arenaria' ),
    
        'separate_items_with_commas' => __( 'Oddzielone', 'Arenaria' ),
    
        'add_or_remove_items'        => __( 'Dodaj lub oddziel elementy', 'Arenaria' ),
    
        'choose_from_most_used'      => __( 'Najpopularniejsze', 'Arenaria' ),
    
        'popular_items'              => __( 'Popularne', 'Arenaria' ),
    
        'search_items'               => __( 'Szukaj', 'Arenaria' ),
    
        'not_found'                  => __( 'Nie znaleziono', 'Arenaria' ),
    
        'no_terms'                   => __( 'Brak elementów', 'Arenaria' ),
    
        'items_list'                 => __( 'Lista elementów', 'Arenaria' ),
    
        'items_list_navigation'      => __( 'Navigacja', 'Arenaria' ),
    
    );
    
    $args = array(
    
        'labels'                     => $labels,
    
        'hierarchical'               => true,
    
        'public'                     => true,
    
        'show_ui'                    => true,
    
        'show_admin_column'          => true,
    
        'show_in_nav_menus'          => true,
    
        'show_tagcloud'              => true,
    
    );
    
    register_taxonomy( 'category_custom', array( 'realizacja' ), $args );
    
    
    
    }
add_action( 'init', 'category_realizacje', 0 );


 // debug for worpdress
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);




 