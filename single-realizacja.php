<?php get_header() ?>


<main class="single-realizacja">

    <section class="section-front-page"
        style="background-image: url(<?php the_field("realizacja_front_page_img");?>)">
        <div class="section-content">
            <div class="section-text-wrapper">
                <div class="section-title">
                    <h1><?php the_field('realizacja_front_page_title');?></h1>
                </div>
                <div class="section-subtitle">
                    <p><?php the_field('realizacja_front_page_subtitle');?></p>
                </div>
                <div class="section-text">
                    <?php the_field('realizacja_front_page_text');?>
                </div>
                <div class="section-button section-buttons transparent">
                    <a href="#section-single-realizaje-main">
                        <button><?php the_field('realizacja_front_page_btn_1');?></button>
                    </a>
                </div>
            </div>
        </div>
        <div class="section-bottom-img">
            <a href="#section-single-realizaje-main">
                <img src="/app/themes/arenaria/assets/src/img/circle-1.png" />
            </a>
        </div>
        <div class="section-breadcrumbs">
            <a href='/realizacje'> Realizacje </a>
            <p> > </p>
            <p> <?php the_title();?></p>
        </div>
        <div class="section-filtr">
            <img src="/app/themes/arenaria/assets/src/img/single-realizacja.png" />
        </div>
    </section>

    <section class="section-single-realizaje-main"
        id="section-single-realizaje-main">
        <div class="section-content">
            <div class="main-top">
                <?php if( have_rows('single_real_top_rep') ): ?>
                <?php while( have_rows('single_real_top_rep') ): the_row();?>
                <div class="block">
                    <div class="block-img">
                        <?php $imageHeroHomepageLeftRight = get_sub_field('single_real_top_img'); ?>
                        <img class="img-hero-homepage"
                            src="<?php echo $imageHeroHomepageLeftRight['sizes']['large']; ?>"
                            width="<?php echo $imageHeroHomepageLeftRight['sizes']['large-width']; ?>"
                            height="<?php echo $imageHeroHomepageLeftRight['sizes']['large-height']; ?>"
                            alt="<?php echo esc_attr($imageHeroHomepageLeftRight['alt']); ?>" />
                    </div>
                    <div class="block-text">
                        <h5><?php the_sub_field("single_real_top_text");?></h5>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="main-center">
                <div class="section-title">
                    <h2><?php the_field('single_real_cent_title');?></h2>
                </div>
                <div class="section-text">
                    <?php the_field('single_real_cent_text');?>
                </div>
            </div>
            <div class="main-gallery">
                <div class="section-title">
                    <h3><?php the_field('single_real_gall_title');?></h3>
                </div>
                <div class="section-gallery">
                    <h3><?php the_field('single_real_gall');?></h3>
                </div>
            </div>
        </div>
    </section>

    <section class="section-middle singlecontent">
        <div class="section-content">
            <?php while(have_posts()) : the_post(); ?>
            <?php the_content();?>
            <?php endwhile; ?>
        </div>
        </sec>

        <section class="section-realizacje">
            <div class="section-content">
                <div class="section-title">
                    <h2><?php the_field('single_real_realizacja_title');?></h2>
                </div>
            </div>
            <div class="section-content realizacje-posts">

                <?php if( have_rows("single_realizacja_rep") ): ?>
                <?php while( have_rows("single_realizacja_rep") ): the_row();?>

                <?php
                                $featured_post = get_sub_field('single_realizacja_post');
                                $permalink = get_permalink( $featured_post->ID );
                                $date = get_the_date( 'Y', $featured_post->ID );
                        ?>

                <div class="post-realizacje-single">
                    <a href="<?php echo esc_url( $permalink ); ?>">
                        <div class="post-realizacje"
                            style="background-image: url(<?php echo get_the_post_thumbnail_url($featured_post->ID ); ?>)">
                            <div class="post post-<?php echo $i; ?>">

                                <div class="post-content">
                                    <div class="content">
                                        <div class="post_date">
                                            <p><?php echo esc_html( $date ); ?></p>
                                        </div>
                                        <div class="post_title">
                                            <h4><?php echo esc_html( $featured_post->post_title ); ?></h4>
                                            <img src="/app/themes/arenaria/assets/src/img/arrow.png" />
                                        </div>
                                        <div class="post_text">
                                            
                                            <?php  echo wp_trim_words( get_the_excerpt($featured_post->ID), 15 ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <?php endwhile; ?>
                <?php endif; ?>

            </div>
            <div class="section-content realizacje-button">
                <div class="section-button">
                    <a href="<?php the_field('single_rel_btn_url');?>">
                        <button><?php the_field('single_rel_btn');?></button>
                    </a>
                </div>
            </div>
        </section>

        <?php get_template_part( 'template-parts/section-bottom-form' ); ?>

</main>

<?php get_footer(); ?>