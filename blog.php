<?php 
/* 
Template Name: Blog
*/ 
?>

<?php get_header("sub") ;?>

<main id="blog">
    <div class="section-title">
        <div class="section-content">
            <h1><?php the_field('blog_title');?></h1>
        </div>
    </div>
    <section class="section-blog section-first">
        <div class="section-content">
            <div class="section-posts">
                <?php
                    // args query
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 1,
                    );

                    // custom query
                    $recent_posts = new WP_Query($args);

                    // check that we have results
                    if($recent_posts->have_posts()) : $i=0; ?>

                <?php 
                    // start loop
                    while ($recent_posts->have_posts() ) : $recent_posts->the_post(); ?>

                <?php $i++;?>

                <a href="<?php the_permalink(); ?>"
                    class="post post-<?php echo $i; ?>">
                    <div class="post-left">
                        <img src="<?php the_post_thumbnail_url('large'); ?>" />
                    </div>
                    <div class="post-right">
                        <div class="post_date">
                            <p><?php echo get_the_date('d/m/y'); ?></p>
                        </div>
                        <div class="post_title">
                            <h5><?php echo get_the_title(); ?></h5>
                        </div>
                        <div class="post_cat">
                        <?php
                            $category_detail = get_the_category(get_the_ID());
                            $cat_arr = [];
                            foreach ($category_detail as $cd)
                            {
                                $cat_arr[] = $cd->cat_name;
                            }
                            ?>
                            <p> <?php echo $cat_arr[0];?></p>
                            <p> <?php echo $cat_arr[1];?></p>
                            <p> <?php echo $cat_arr[2];?></p>
                        </div>
                        <div class="post_text">
                            <?php// echo the_excerpt(); ?>
                            <?php  echo wp_trim_words( get_the_excerpt(), 35 ); ?>
                        </div>
                        <div class="section-button">
                            <button>zobacz więcej</button>
                        </div>
                    </div>
                </a>


                <?php endwhile; ?>
                <?php wp_reset_query();?>
                <?php endif;?>
            </div>
        </div>
    </section>
    <div class="section-search-form">
        <div class="section-content">
            <?php the_field('blog_search_form');?>
        </div>
    </div>
</main>

<?php get_footer(); ?>